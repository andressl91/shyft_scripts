import tempfile
from pathlib import Path

import shyft.time_series as sa

# Create a temporary directory for Dtss, to store TimeSeries
tmp_dir = Path(tempfile.mkdtemp())
dtss_example_folder = tmp_dir / Path("example")
dtss_example_folder.mkdir(exist_ok=True)

dtss = sa.DtsServer()
dtss_port = 30000
dtss.set_listening_port(dtss_port)

# Set maximum cache size of Dtss
dtss.cache_memory_target = 5 * 1000 * 1000 * 1000  # 5GB

# or Maximum timeseries by count
# dtss.cache_max_items = 1000

# Create a container where we can store shyft timeseries
# For this example, any TimeSeries-url like shyft://spam/* would be stored in this path
# See crate_and_store_values method
dtss.set_container("spam", dtss_example_folder.as_posix())
dtss.start_async()

# www_dir = tmp_dir / Path("example")
# www_dir.mkdir(exist_ok=True)
# dtss.start_web_api(host_ip='0.0.0.0',
#                   port=30001,
#                   doc_root=www_dir.as_uri())

t_now = sa.utctime_now()  # Get UTC time


def create_and_store_values_to_dtss():
    cal = sa.Calendar("Europe/Oslo")
    ta = sa.TimeAxis(t_now,  # Start
                     cal.DAY,  # Interval-size
                     3)  # N-intervals

    """ Create timeseries with all values 3, intepret intervales as average values
     14 |                                  * -----------
        |                                  |
     12 |                 * ______________
        |                |
     10 |  * ____________| 
        __________________________________________________
          t_now    t_now + cal.DAY      t_now + 2*cal.DAY
    """
    ts1 = sa.TimeSeries(ta, [10, 12, 14], point_fx=sa.POINT_AVERAGE_VALUE)
    print(ts1(t_now + cal.DAY))
    # Get first value
    assert ts1(t_now) == 10  # First point
    assert ts1(t_now + cal.HOUR * 12) == 10  # Middle point in first interval
    # Name the timeseries for storing backend
    ts1 = sa.TimeSeries("shyft://spam/time_series_1.db", ts1)

    """ Create timeseries with all values 3, intepret intervales as intant values

     3 |                      *
       |                   -
       |                -                       
     2 |             * 
       |          -
       |       -
     1 |    *
        ____|________|__________|___________________
         t_now       + cal.DAY  + 2*cal.DAY
    """
    ts2 = sa.TimeSeries(ta, [1, 2, 3], point_fx=sa.POINT_INSTANT_VALUE)
    ts2 = sa.TimeSeries(ts_id="shyft://spam/time_series_2.db", bts=ts2)
    assert ts2(t_now) == 1  # First point
    assert ts2(t_now + cal.HOUR * 12) == (1 + 2) / 2.0  # Middle point in first interval
    # Name the timeseries for storing backend
    ts1 = sa.TimeSeries("shyft://spam/time_series_1.db", ts1)

    tsv = sa.TsVector()
    tsv.append(ts1)
    tsv.append(ts2)

    dtsc = sa.DtsClient(f"localhost:{dtss_port}")
    dtsc.store_ts(tsv, overwrite_on_write=True, cache_on_write=True)

    # Our timeseries stored at "backend"
    for ts_files in dtss_example_folder.glob("*"):
        print(ts_files)


def read_from_dtss():
    cal = sa.Calendar("Europe/Oslo")
    ta = sa.TimeAxis(t_now,  # Start
                     cal.DAY,  # Interval-size
                     3)  # N-intervals

    ts1 = sa.TimeSeries(ts_id="shyft://spam/time_series_1.db")
    ts2 = sa.TimeSeries(ts_id="shyft://spam/time_series_2.db")
    ts3 = ts1 * ts2
    tsv = sa.TsVector()
    tsv.extend([ts1, ts2, ts3])

    dtsc = sa.DtsClient(f"localhost:{dtss_port}")
    read_period = ta.total_period()

    # Dts-client request, give me the following series in this period
    tsv_readback = dtsc.evaluate(tsv, read_period)
    first_ts = tsv_readback[0]
    second_ts = tsv_readback[1]

    # Lets see if the series are the same
    assert first_ts(t_now) == 10.0  # First point
    assert first_ts(t_now + cal.HOUR * 12) == 10  # Middle point in first interval

    assert second_ts(t_now) == 1  # First point
    assert second_ts(t_now + cal.HOUR * 12) == (1 + 2) / 2.0  # Middle point in first interval (1.5)


def serialize():
    cal = sa.Calendar("Europe/Oslo")
    ta = sa.TimeAxis(t_now,  # Start
                     cal.DAY,  # Interval-size
                     3)  # N-intervals

    ts1 = sa.TimeSeries(ta, [10, 12, 14], point_fx=sa.POINT_AVERAGE_VALUE)
    ts1 = sa.TimeSeries("shyft://spam/time_series_1.db", ts1)

    with tempfile.TemporaryDirectory() as tmp:
        tmp_dir = Path(tempfile.mkdtemp())
        bin_file = Path(tmp_dir)/Path("time_series.db")
        bin_file.touch()
        c = bytes(str(ts1.serialize()), 'utf-8')
        bin_file.write_bytes(c)
        d = bin_file.read_bytes()
        e = 2
        #bin_file.write_bytes(ts1.serialize())
        bytes()




create_and_store_values_to_dtss()
read_from_dtss()
serialize()
dtss.clear()  # Stop dtss
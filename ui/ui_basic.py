import asyncio
import time
from tempfile import TemporaryDirectory

import shyft.energy_market.ui as sui
import websockets



def api_host(port):
    return f"ws://127.0.0.1:{port}"


def get_response(req: str, port: int):
    async def wrap(wreq):
        async with websockets.connect(api_host(port)) as websocket:
            await websocket.send(wreq)
            response = await websocket.recv()
            return response

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    return loop.run_until_complete(wrap(req))

def get_layout_request(port):
    print("###########################################")
    print("get_layout request over web-api\n")
    req = """get_layouts {"request_id": "example-store" } """
    print(f"request =>   {req}", "\n")
    res = get_response(req, port)
    print(f"response => {res}\n")
    print("###########################################")

def read_layout_request(port):
    print("###########################################")
    print("read_layout request over web-api\n")
    req = """read_layout {"request_id": "example-read2", "layout_id": 1} """
    print(f"request =>   {req}", "\n")
    res = get_response(req, port)
    print(f"response => {res}")
    print("###########################################")

def read_layout_id3_request(port):
    print("###########################################")
    print("read_layout request over web-api\n")
    req = """read_layout {"request_id": "example-read3", "layout_id": 3} """
    print(f"request =>   {req}", "\n")
    res = get_response(req, port)
    print(f"response => {res}")
    print("###########################################")

def store_layout_id3_request(port):
    print("###########################################")
    print("store_layout request over web-api\n")
    req_esc = """store_layout {"request_id": "example-store", 
        "layout_id": 3,
        "name": "my_layout",
        "json": {
            \"foo\": \"bar\"
        }
    } """
    req = """store_layout {"request_id": "example-store", 
        "layout_id": 3,
        "name": "my_layout",
        "json": {
            "foo": "bar"
        }
    } """
    print(f"request =>   {req_esc}", "\n")
    res = get_response(req_esc, port)
    print(f"response => {res}")
    print("###########################################")

with TemporaryDirectory() as tmp:
    srv = sui.LayoutServer(tmp)
    port = srv.start_server()
    web_port = port + 1
    srv.start_web_api("127.0.0.1",
                      web_port,
                      doc_root=tmp,
                      fg_threads=1,
                      bg_threads=1)
    print(f"Starting server on {port} and web_api on {web_port}")

    lf = sui.LayoutInfo(id=1, name="test", json="{}")
    mi = sui.ModelInfo(id=lf.id, name=lf.name, created=0, json="")

    client = sui.LayoutClient(f"127.0.0.1:{port}", 999)
    client.store_model(lf, mi)
    read_layout_request(web_port)

    #mis = client.get_model_infos([])
    #print([(mi.id, mi.name) for mi in mis])
    get_layout_request(web_port)

    store_layout_id3_request(web_port)
    get_layout_request(web_port)
    mis = client.get_model_infos([])
    print([(mi.id, mi.name, mi.json) for mi in mis])

    print("Read model 3 from python")
    m = client.read_model(3)
    print(m.id, m.json)

    read_layout_id3_request(web_port)

    time.sleep(2)

    srv.stop_web_api()
    srv.stop_server()
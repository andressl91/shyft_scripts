import logging
from typing import Optional, Union, List

from pint import UnitRegistry
from shyft.dashboard.time_series.sources.source import DataSource

from shyft.time_series import UtcPeriod, DtsClient, TsVector, TimeSeries, TimeAxis

from shyft.dashboard.time_series.sources.ts_adapter import BasicTsAdapter, TsAdapter
from shyft.dashboard.base.ports import Receiver
from shyft.dashboard.time_series.view_container.figure import Figure
from shyft.dashboard.time_series.view_container.legend import Legend
from shyft.dashboard.time_series.view_container.table import Table
from shyft.dashboard.time_series.ds_view_handle import DsViewHandleCreator, DsViewHandle
from shyft.dashboard.time_series.view import Line

from dtss.dtss import get_port

from pint import Unit, Quantity

class State:
    unit_registry = UnitRegistry()
    unit_registry.define("EUR = [euro]")
    unit_registry.define("NOK = [norske_kroner]")
    unit_registry.define("euro = EUR")
    Quantity = unit_registry.Quantity
    unit_convert = None


class DtssTsAdapter(TsAdapter):

    def __init__(self, dtss_url: str, ts_url: str, unit_registry, unit: str='') -> None:
        super().__init__()
        # TODO add unit reg
        self.dtsc = DtsClient(dtss_url)
        self.tsv_request = TsVector([TimeSeries(ts_url)])
        self.unit_registry = unit_registry
        self.unit = unit

    def __call__(self, *, time_axis: TimeAxis, unit: Unit) -> Quantity[TsVector]:
        try:
            tsv = self.dtsc.evaluate(self.tsv_request.average(time_axis), time_axis.total_period())
            print("")

        except RuntimeError as e:
            return TsVector()
        #finally:
        #    self.dtsc.close()
        return self.unit_registry.Quantity(tsv, self.unit)



class TickerDsvhCreator(DsViewHandleCreator):
    def __init__(self,
                 unit_registry: Optional[UnitRegistry] = None,
                 figure_container: Optional[Union[List[Figure], Figure]] = None,
                 legend_container: Optional[Union[List[Legend], Legend]] = None,
                 table_container: Optional[Union[List[Table], Table]] = None,
                 logger: Optional[logging.Logger] = None) -> None:
        super().__init__(unit_registry=unit_registry,
                         figure_container=figure_container,
                         legend_container=legend_container,
                         table_container=table_container,
                         logger=logger)

        self.receive_data = Receiver(parent=self,
                                     name='receive module',
                                     signal_type=List[str],
                                     func=self._receive_data)
        self.reference_color = "black"

    def create_ds_view_handles(self, ticker) -> List[DsViewHandle]:
        dsvhs = list()
        print("In create_ds_view_handles", ticker)
        dsvhs.extend(self.create_instrument_dsvhs(ticker[0]))

        return dsvhs

    def create_instrument_dsvhs(self, ticker) -> List[DsViewHandle]:
        unit = ''
        ref_ts = ticker.hist
        tsa = BasicTsAdapter(data=ref_ts, unit_registry=self.unit_registry, unit=unit)
        time_range = UtcPeriod(int(ref_ts.time_axis.time_points.min()) - 1,
                               int(ref_ts.time_axis.time_points.max()) + 1)
        label = f"{ticker.name}_hist"
        data_source = DataSource(ts_adapter=tsa, unit=tsa.unit, tag=label, time_range=time_range)
        views = self.get_views(unit=unit,
                               label=label,
                               y_axis_label='Price',
                               color=self.reference_color)
        dsvh = DsViewHandle(data_source=data_source, views=views, tag=label, unit_registry=self.unit_registry)
        #return [dsvh]

        # Can add multiple
        label = f"{ticker.name}_high"

        dtss_tsa = DtssTsAdapter(dtss_url=f"127.0.0.1:{get_port()}", ts_url=ticker.high.ts_id(),
                                 unit_registry=self.unit_registry, unit=unit)
        data_source2 = DataSource(ts_adapter=dtss_tsa, unit=unit, tag=label, time_range=time_range)
        views2 = self.get_views(unit=unit,
                               label=label,
                               y_axis_label='Price',
                               color="green")

        dsvh1 = DsViewHandle(data_source=data_source2, views=views2, tag=label, unit_registry=self.unit_registry)
        return [dsvh, dsvh1]

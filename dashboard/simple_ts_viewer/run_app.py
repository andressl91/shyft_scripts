import logging
from app import TsApp

from shyft.dashboard.apps.start_bokeh_apps import start_bokeh_apps
from shyft.dashboard.util.find_free_port import find_free_port

if __name__ == "__main__":
    import argparse

    available_apps = {'blueruby': TsApp}

    # add user name as in old bat scripts from sih
    default_port = find_free_port()

    parser = argparse.ArgumentParser(prog="start_bokeh_apps",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-s", "--no-show", action="store_false", default=True,
                        help="Don't show bokeh apps in default browser")
    parser.add_argument("-a", "--apps", nargs="+",
                        default=list(available_apps.keys()),
                        dest='apps', help=f"Run one or more specific app(s): {list(available_apps.keys())}")
    parser.add_argument("-p", "--port", type=int, default=default_port,
                        help="Port to run bokeh server [will crash if it is not free]")
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.ERROR)
    parser.add_argument('-v', '--verbose', help="Be verbose",
                        action="store_const", dest="log_level", const=logging.INFO)
    parser.add_argument("-l", "--logger_box", action="store_true", default=False,
                        help="Enable logger box in application")
    parser.add_argument("-u", "--use-external-dtss", action="store_true", dest="use_external_dtss", default=False,
                        help="Use external dtss, do *not* start local dtss for demo purposes")

    a = parser.parse_args()

    if not a.apps:
        raise RuntimeError(f"No apps defined, use -a to define apps! Possible apps are: {list(available_apps.keys())}")

    apps = [available_apps[k] for k in a.apps if k in available_apps]
    if not apps:
        raise RuntimeError(
            f"'-a : {a.apps}' unknown apps defined! possible apps are: {list(available_apps.keys())}")

    app_kwargs = {}
    start_bokeh_apps(apps=apps,
                     show=a.no_show,
                     port=a.port,
                     log_level=a.log_level,
                     show_logger_box=a.logger_box, app_kwargs=app_kwargs)

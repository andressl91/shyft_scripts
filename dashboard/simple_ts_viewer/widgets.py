import logging
from concurrent.futures.thread import ThreadPoolExecutor
from typing import Optional

import bokeh.layouts

from bokeh.document import Document
from bokeh.layouts import row, column
from bokeh.models import Spacer
from shyft.dashboard.time_series.ds_view_handle_registry import DsViewHandleRegistryApp

from shyft.dashboard.time_series.axes import YAxis, YAxisSide
from shyft.dashboard.time_series.renderer import LineRenderer

from shyft.dashboard.time_series.tools.figure_tools import ResetYRange, WheelZoomDirection, HoverTool

from shyft.dashboard.time_series.tools.ts_viewer_tools import ResetTool

from shyft.dashboard.time_series.tools.view_time_axis_tools import ViewPeriodSelector

from plotting_functions import TickerDsvhCreator
from shyft.dashboard.base.ports import connect_ports, States, StatePorts, connect_state_ports

from shyft.dashboard.base.selector_model import BasicSelector

from shyft.dashboard.base.selector_presenter import SelectorPresenter

from shyft.dashboard.base.selector_views import Select


from shyft.dashboard.time_series.ts_viewer import TsViewer
from shyft.dashboard.time_series.view_container.figure import Figure

from shyft.dashboard.base.app import Widget

import shyft.time_series as sa
from shyft.dashboard.time_series.state import State
from dtss.dtss import shyft_url_gen, get_port, get_ta

class Ticker:
    def __init__(self, name: str, hist: sa.TimeSeries, high: sa.TimeSeries):
        self.name = name
        self.hist = hist
        self.high = high

def create_tickers():

    dtsc = sa.DtsClient(f"127.0.0.1:{get_port()}")
    tickers = []

    ts = sa.TimeSeries(shyft_url_gen("ticker", "hist"))
    tsv = sa.TsVector()
    tsv.append(ts)
    tsv_get = dtsc.evaluate(tsv, get_ta().total_period())
    # Bound!
    ts_hist = tsv_get[0]
    # Unbound!
    ts_high = sa.TimeSeries(shyft_url_gen("ticker", "high"))
    tickers.append(Ticker(name="NOK", hist=ts_hist, high=ts_high))

    return tickers


class TsAppWidget(Widget):
    def __init__(self, *,
                 bokeh_document: Document,
                 logger: Optional[logging.Logger] = None,
                 thread_pool_executor: Optional[ThreadPoolExecutor] = None):
        super().__init__(logger=logger)
        self.bokeh_document = bokeh_document
        unit_registry = State.unit_registry
        toolbar_height = 80

        ## Minimal example for showing text in a dropdown, doesn't need anything from plotting_fuctions
        self.tickers = create_tickers()
        self.ticker_selector_view = Select(title='Ticker Selector')
        self.ticker_presenter = SelectorPresenter(name='Ticker presenter',
                                                      view=self.ticker_selector_view)
        # Note, it is "i" in set_selector_options which is sent, when user selects i.name (clear later in connect ports)
        self.ticker_presenter.set_selector_options([(i, i.name) for i in self.tickers])
        self.ticker_selector = BasicSelector(presenter=self.ticker_presenter)

        #self._layout = column(self.ticker_selector_view.layout)
        ###

        t0 = sa.utctime_now()
        cal = sa.Calendar("Europe/Oslo")
        t_month = cal.trim(t0, cal.MONTH)
        period = sa.UtcPeriod(t_month, t0)

        # Initial view range of plot
        init_view_range = sa.UtcPeriod(t0 - 30 * sa.Calendar.DAY, t0 + sa.Calendar.DAY)
        time_step_restrictions = [sa.Calendar.HOUR,
                                  sa.Calendar.HOUR * 2,
                                  sa.Calendar.HOUR * 3,
                                  sa.Calendar.HOUR * 4,
                                  sa.Calendar.HOUR * 6,
                                  sa.Calendar.HOUR * 8,
                                  sa.Calendar.HOUR * 12,
                                  sa.Calendar.DAY,
                                  sa.Calendar.WEEK,
                                  sa.Calendar.MONTH]


        view_period_selector = ViewPeriodSelector()
        reset_tool = ResetTool()
        # Main class for combining components to view timeseries
        # |  # create our viewer app
        # | viewer = TsViewer()
        #|
        # |  # create view container
        # | table1 = Table(viewer=viewer)
        #| fig1 = Figure(viewer=viewer)

        self.viewer = TsViewer(bokeh_document=self.bokeh_document, title='Time resolution',
                               unit_registry=unit_registry, tools=[reset_tool, view_period_selector],
                               init_view_range=init_view_range,
                               add_dt_selector=True,
                               time_step_restrictions=time_step_restrictions,
                               thread_pool_executor=thread_pool_executor)




        reset_y_range_tool = ResetYRange()
        wheel_zoom = WheelZoomDirection()


        layout_spacer = Spacer(height=20, width=100, sizing_mode='fixed')

        # Tools for zooming or setting time view of timeseries
        # Edit widget_toolbar list to see in bokeh app what spawns
        self.widget_toolbar = row(self.viewer.dt_view.layout, # For selecting dt of timeseries
                                  column(layout_spacer, reset_tool.layout, width=100, height=toolbar_height), # The reset tool, to get initial state
                                  view_period_selector.layout, # For selecting shift period of data
                                  column(layout_spacer, reset_y_range_tool.layout, width=140, height=toolbar_height), # Reset y-range to initial state
                                  column(layout_spacer, wheel_zoom.layout, width=160, height=toolbar_height), # Selctor for scrolling in x-dir or y-dir
                                  height=toolbar_height
                                  )
        #self._layout = self.widget_toolbar
        #

        ## Making of Figure showing ts
        # Define y axis of timeseriesplot
        y_axes_dict = {'Price': ''}
        y_axes = [YAxis(label=label, unit=unit, side=YAxisSide.LEFT, default_y_range=(0, 1000) if i == 0 else (0, 1))
                  for i, (label, unit) in enumerate(y_axes_dict.items())] if y_axes_dict else None
        # Figure is a view container for a figure
        hover_tool = HoverTool(formatters={'$x': 'datetime'})
        self.figure = Figure(viewer=self.viewer, title=' ', tools=[reset_y_range_tool, wheel_zoom, hover_tool],
                             y_axes=y_axes, show_x_axis_label=False, title_text_font_size=20,
                             init_renderers={LineRenderer: 6})

        layout_spacer = Spacer(height=20, width=20, sizing_mode='fixed')

        self.widget_layout = row(layout_spacer,
                                 self.figure.layout, # Figure for showing timeseries
                                 )


        #self._layout = self.widget_layout
        ###############################################


        self.dsvh_registry_app = DsViewHandleRegistryApp(logger=logger)

        # Rememer this is defined by the lib user, creating a list of ds_view_handles
        self.inst_dsvh = TickerDsvhCreator(figure_container=self.figure)

        # Connect the prots

        # Send selected ticker from dropdown to InstrumentDsvhCreator, taking the Figure instance in constructor
        connect_ports(self.ticker_selector.send_selection, self.inst_dsvh.receive_data)
        # Send ds_view_handles from client defined TickerDsvhCreator, and send to DsViewHandleRegistryApp
        connect_ports(self.inst_dsvh.send_ds_view_handles, self.dsvh_registry_app.receive_ds_view_handles_to_register)
        # Send added ds_view_handels to the viewer (which is the TsViewer, showing the TimeSeries data in the ds_view_handles
        connect_ports(self.dsvh_registry_app.send_ds_view_handles_to_add, self.viewer.receive_ds_view_handles_to_add)

        from bokeh.models import Paragraph
        p = Paragraph(text="""Your text is initialized with the 'text' argument.  The
        remaining Paragraph arguments are 'width' and 'height'. For this example, those values
        are 200 and 100, respectively.""",
                      width=300, height=100)

        self._layout = column(p, self.ticker_selector_view.layout, self.widget_toolbar, self.widget_layout)
        self._layout.name = str(self._layout.id)

    @property
    def layout(self):
        return self._layout

    @property
    def layout_components(self):
        return {"widgets": [],
                "figures": []}

    def _receive_state(self, state: States) -> None:
        if state == self._state:
            return
        self._state = state
        self.state_port.send_state(state)




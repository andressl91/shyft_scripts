from concurrent.futures.thread import ThreadPoolExecutor
from typing import Optional, Dict, Any

import bokeh

from shyft.dashboard.widgets.logger_box import LoggerBox

from shyft.dashboard.base.app import AppBase

from widgets import TsAppWidget


class TsApp(AppBase):

    def __init__(self, thread_pool: Optional[ThreadPoolExecutor] = None,
                 app_kwargs: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(thread_pool=thread_pool)

    @property
    def name(self) -> str:
        """
        This property returns the name of the app
        """
        return "Blueruby App"

    def get_layout(self, doc: 'bokeh.document.Document', logger: Optional[LoggerBox] = None) -> bokeh.models.LayoutDOM:
        """
        This function returns the full page layout for the app
        """
        doc.title = self.name

        widget = TsAppWidget(bokeh_document=doc, thread_pool_executor=self.thread_pool, logger=logger)

        return widget.layout




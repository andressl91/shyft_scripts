from pathlib import Path
from tempfile import TemporaryDirectory
import shyft.time_series as sa

def shyft_url_gen(container: str, path: str):
    return f"shyft://{container}/{path}"

def get_port():
    return 60006

def get_ta():
    cal = sa.Calendar("Europe/Oslo")
    t_now = sa.utctime_now()
    t_day = cal.trim(t_now, cal.DAY)
    ta = sa.TimeAxis(t_day, cal.DAY, 6)
    return ta

if __name__ == "__main__":
    cal = sa.Calendar("Europe/Oslo")
    t_now = sa.utctime_now()
    t_day = cal.trim(t_now, cal.DAY)

    ta = sa.TimeAxis(t_day, cal.DAY, 6)
    ts = sa.TimeSeries(ta, [1, 5, 3, 4, 10, 9], point_fx=sa.POINT_INSTANT_VALUE)
    #ts = sa.TimeSeries(shyft_url_gen("spam", "egg.tsb"), ts)
    ts = sa.TimeSeries("shyft://ticker/hist", ts)

    ts_spam = sa.TimeSeries(ta, [2, 3, 5, 7, 11, 15], point_fx=sa.POINT_INSTANT_VALUE)
    ts_spam = sa.TimeSeries("shyft://ticker/high", ts_spam)

    tsv = sa.TsVector()
    tsv.append(ts)
    tsv.append(ts_spam)

    with TemporaryDirectory() as tmp:
        root = Path(tmp)
        ticker_dir = root / Path("ticker")
        dtss = sa.DtsServer()
        dtss.set_container("ticker", ticker_dir.as_posix())
        #port = dtss.start_async()
        port = get_port()
        dtss.set_listening_port(port)
        dtss.start_async()


        dtsc = sa.DtsClient(f"localhost:{port}")
        dtsc.store_ts(tsv)

        while True:
            pass
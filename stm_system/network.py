from shyft.time_series import TimeSeries, TimeAxis, Calendar, utctime_now, POINT_AVERAGE_VALUE
from shyft.energy_market.stm import StmSystem, MarketArea, PowerModule, Network, Busbar, TransmissionLine
from functools import reduce


def create_network_with_powermodule(stm_system: StmSystem):
    # Create mock TimeAxis for creating of TimeSeries, to show off attrs on objects
    cal = Calendar("Europe/Oslo")
    dt = cal.DAY
    start = cal.trim(utctime_now(), cal.WEEK)
    end = cal.add(start, cal.WEEK, 1)
    n = cal.diff_units(start, end, dt)
    ta = TimeAxis(start, dt, n)

    # Each StmSystem can have one or more Networks
    n = Network(1, "NO1_Network", "{}", sys)
    sys.networks.append(n)

    # A Network is build up of transmissionlines, going
    # from one BusBar to another BusBar
    tl1 = TransmissionLine(1, "TL1", "{}", n)
    n.transmission_lines.append(tl1)
    b1 = Busbar(1, 'Busbar #1', '{}', n)
    b2 = Busbar(2, 'Busbar #2', '{}', n)
    n.busbars.extend([b1, b2])

    # Associate busbar to start of transmission line
    b1.add_to_start_of_transmission_line(tl1)
    assert b1.get_transmission_lines_from_busbar()[0] == tl1
    assert len(b1.get_transmission_lines_to_busbar()) == 0
    assert tl1.from_bb.id == 1
    # Associate busbar to end of transmission line
    b2.add_to_end_of_transmission_line(tl1)
    assert len(b2.get_transmission_lines_from_busbar()) == 0
    assert b2.get_transmission_lines_to_busbar()[0] == tl1
    assert tl1.to_bb.id == 2

    # Now associate the BusBar from the start of the TransmissionLine
    # to the PowerModule
    pm1 = PowerModule(1, "PM1", "{}", sys)
    pm2 = PowerModule(1, "PM2", "{}", sys)
    stm_system.power_modules.extend([pm1, pm2])

    # Historical values
    pm1.power.realised.value = TimeSeries(ta, fill_value=2, point_fx=POINT_AVERAGE_VALUE)
    # Power Consumption/Production scheduled for the future
    pm1.power.schedule.value = TimeSeries(ta, fill_value=2, point_fx=POINT_AVERAGE_VALUE)
    # Attr for holding simulation results
    pm1.power.result.value = TimeSeries(ta, fill_value=2, point_fx=POINT_AVERAGE_VALUE)

    b1.add_to_power_module(pm1)
    b1.add_to_power_module(pm2)
    # Verify PowerModule is indeed connected to a BusBar
    assert pm1.connected.id == b1.id
    assert pm1.connected.name == b1.name

    # Check that both PowerModules are connected to the Bus
    pms = b1.get_power_modules()
    assert pms[0].name == pm1.name
    assert pms[1].name == pm2.name


def two_market_areas_connected(stm_system: StmSystem):
    # Not used in the example, just for illustration
    no1 = MarketArea(1, "NO1", "{}", stm_system)
    no2 = MarketArea(1, "NO2", "{}", stm_system)

    """
    |       |           |       |
    |  NO1  |   ---- >  |  NO2  |
    |       |  < -----  |       |
    """

    no1_network = Network(1, "NO1_Network", "{}", stm_system)
    no2_network = Network(2, "NO2_Network", "{}", stm_system)
    stm_system.networks.extend([no1_network, no2_network])

    no1_no2_line = TransmissionLine(1, "NO1_NO2", "{}", no1_network)
    no2_no1_line = TransmissionLine(1, "NO2_NO1", "{}", no2_network)

    no1_network.transmission_lines.append(no1_no2_line)
    no2_network.transmission_lines.append(no2_no1_line)

    no1_bus = Busbar(1, "NO1_Bus", "{}", no1_network)
    no2_bus = Busbar(2, "NO2_Bus", "{}", no2_network)
    no1_network.busbars.append(no1_bus)
    no2_network.busbars.append(no2_bus)

    no1_bus.add_to_start_of_transmission_line(no1_no2_line)
    no1_bus.add_to_end_of_transmission_line(no2_no1_line)

    no2_bus.add_to_start_of_transmission_line(no2_no1_line)
    no2_bus.add_to_end_of_transmission_line(no1_no2_line)

    # Visually see that TransmissionLine is connected to the right busses
    print(f"{no1_no2_line.name} is connected from {no1_no2_line.from_bb} to {no1_no2_line.to_bb}")
    print(f"{no2_no1_line.name} is connected from {no2_no1_line.from_bb} to {no2_no1_line.to_bb}")


    print(no1_bus.get_transmission_lines_from_busbar())
    print(no1_bus.get_transmission_lines_to_busbar())
    print(no2_bus.get_transmission_lines_from_busbar())
    print(no2_bus.get_transmission_lines_to_busbar())

    # Walk from NO1 to NO2 by attrs
    print(f"Start at {no1_bus.name} find line {no1_bus.get_transmission_lines_from_busbar()[0].name} "
          f"the line end up at {no1_bus.get_transmission_lines_from_busbar()[0].to_bb.name}")

if __name__ == "__main__":
    sys = StmSystem(1, "stm_system", "{}")
    create_network_with_powermodule(sys)
    two_market_areas_connected(sys)

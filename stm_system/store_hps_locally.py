from pathlib import Path
from shyft.energy_market.stm import StmSystem, HydroPowerSystem
from shyft.energy_market.stm.model_repository import HydroPowerSystemRepository

def create_and_store_hps():
    stm = StmSystem(1, "stm_here", "")
    hps = HydroPowerSystem(1, "hps_there")
    
    stm.hydro_power_systems.append(hps)
    ms = HydroPowerSystemRepository(Path("./store"))
    ms.save_model(hps)


if __name__ == "__main__":
    create_and_store_hps()

import json
from tempfile import TemporaryDirectory
from pathlib import Path
from shyft.energy_market.stm import DStmServer, DStmClient, StmSystem, HydroPowerSystem, ModelInfo
import shyft.time_series as sa

#with TemporaryDirectory as tmp:



def create_simple_stm(name):
    stm = StmSystem(uid=name, name="stm1", json="json")
    hps = HydroPowerSystem(uid=1, name="hps1")
    res = hps.create_reservoir(uid=1,name="res1", json="json")



class MyCallback:

    def __init__(self, dstm_service):
        self.dstm_service = dstm_service



    def __call__(self, mdl_id: str, command: str):
        if command == "hello":
            print("Hello")
        if command == "print_name":
            model = self.dstm_service.do_get_model(mdl_id)



dstm_server = DStmServer()
dstm_server.fx = MyCallback(dstm_server)

port = dstm_server.start_server()

stm = create_simple_stm(1)
mi = ModelInfo(id=1, name="stm1_run1", created=sa.utctime_now(), json="")


dstm_client = DStmClient(f"127.0.0.1:{port}", 9999)
#dstm_client.add_model("foo", stm)
#get_m = dstm_client.get_model("foo")
#print(get_m.uid)


dstm_client.fx("1", "hello")


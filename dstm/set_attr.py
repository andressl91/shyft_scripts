import time
from pathlib import Path
from tempfile import TemporaryDirectory
from shyft.energy_market.stm import DStmServer, DStmClient, HydroPowerSystem, ModelInfo, StmSystem
import shyft.time_series as sa
from shyft.energy_market.stm._stm import _ts

cal = sa.Calendar("Europe/Oslo")
t_now = sa.utctime_now()
t_start = cal.trim(t_now, cal.DAY)
t_end = cal.add(t_start, cal.HOUR, 5)
dt = cal.HOUR
n = cal.diff_units(t_start, t_end, dt)
ta = sa.TimeAxis(t_start, dt, n)

def create_stm():
    hps = HydroPowerSystem(1, "hps")
    hps.create_reservoir(1, "res")
    res = hps.reservoirs[0]
    res.volume.schedule.value = sa.TimeSeries("shyft://stm/hps/res.db") * 0.5 # ts.is_bound()
    res.inflow.schedule.value = sa.TimeSeries(ta, fill_value=20, point_fx=sa.POINT_AVERAGE_VALUE)

    stm = StmSystem(1, "stm", "{}")
    stm.hydro_power_systems.append(hps)
    return stm

def run_server():
    with TemporaryDirectory() as tmp:
        container = Path(tmp) / Path("spam")
        container.mkdir()
        server = DStmServer()
        port = server.start_server()
        print(f"Started DSTm server: running on 127.0.0.1:{port}")
        remote_dtss = False
        if remote_dtss:
            #print(f"Use remote dtss, forward requests to 127.0.0.1:{dtss_port}")
            #server.set_master_slave_mode(ip="127.0.0.1",
            #                             port=dtss_port,
            #                             master_poll_time=1,
            #                             unsubscribe_threshold=2,
            #                             unsubscribe_max_delay=2)
            time.sleep(2)

        else:
            server.add_container("stm", container.as_posix())

        while True:
            print(f"DSTM running on port {port}")
            time.sleep(5)

def dstm_url(mdl_key: str, attr: _ts, attr_template_levels=-1) -> str:
    attr_url = attr.url(f"dstm://M{mdl_key}", template_levels=attr_template_levels)
    return attr_url.replace("u{parent_id}", "u{o_id}")
def run_attr_edit():

        port = 32859
        stm = create_stm()
        dstm_client = DStmClient(f"127.0.0.1:{port}", 9999)
        print("Add model to DSTM server")
        dstm_client.remove_model(stm.name)
        dstm_client.add_model(stm.name, stm)
        print("Got here")
        time.sleep(1)

        print("Check values of inflow.schedule values pre set_ts function call")
        stm_get = dstm_client.get_model(stm.name)
        print(stm_get.hydro_power_systems[0].reservoirs[0].inflow.schedule.value.v)

        attr = stm_get.hydro_power_systems[0].reservoirs[0].inflow.schedule
        dstm_result  = dstm_url(stm.name, attr)
        print("dstm url for reservoir.inflow.schedule ->  ", dstm_result)

        new_ts = sa.TimeSeries(ta,  fill_value=30, point_fx=sa.POINT_AVERAGE_VALUE)
        new_ts = sa.TimeSeries(dstm_result, new_ts)
        tsv = sa.TsVector()
        tsv.append(new_ts)
        dstm_client.set_ts(stm.name, tsv)
        print("Check values of inflow.schedule values after set)ts function call")
        stm_get = dstm_client.get_model(stm.name)
        print(stm_get.hydro_power_systems[0].reservoirs[0].inflow.schedule.value.v)

        print("Read just the attrs you need with get_ts")
        ts = sa.TimeSeries(dstm_result)
        svec = sa.StringVector()
        svec.append(dstm_result)
        get_result = dstm_client.get_ts(stm.name, svec)
        print(get_result[0].v)

if __name__ == "__main__":
    #run_server()
    run_attr_edit()
import time
from pathlib import Path
from tempfile import TemporaryDirectory
from shyft.energy_market.stm import DStmServer, DStmClient, HydroPowerSystem, ModelInfo, StmSystem
import shyft.time_series as sa

cal = sa.Calendar("Europe/Oslo")
t_now = sa.utctime_now()
t_start = cal.trim(t_now, cal.DAY)
t_end = cal.add(t_start, cal.DAY, 5)

def create_stm():
    hps = HydroPowerSystem(1, "hps")
    hps.create_reservoir(1, "res")
    res = hps.reservoirs[0]
    res.volume.schedule.value = sa.TimeSeries("shyft://stm/hps/res.db") * 0.5 # ts.is_bound()

    stm = StmSystem(1, "stm", "{}")
    stm.hydro_power_systems.append(hps)
    return stm


with TemporaryDirectory() as tmp:
    container = Path(tmp) / Path("spam")
    container.mkdir()

    dtss = sa.DtsServer()
    dtss.set_container("stm", container.as_posix())
    dtss_port = dtss.start_async()

    dtsc = sa.DtsClient(f"127.0.0.1:{dtss_port}")
    ta = sa.TimeAxis(t_start, cal.DAY, 3)
    ts = sa.TimeSeries(ta, fill_value=2, point_fx=sa.POINT_AVERAGE_VALUE)
    ts = sa.TimeSeries("shyft://stm/hps/res.db", ts)
    tsv = sa.TsVector()
    tsv.append(ts)
    dtsc.store_ts(tsv, cache_on_write=True)

    ts_get = sa.TimeSeries("shyft://stm/hps/res.db")
    tsv_get = sa.TsVector()
    tsv_get.append(ts_get)
    res = dtsc.evaluate(tsv, ta.total_period())

    print(dtsc.cache_stats.id_count)


    server = DStmServer()
    port = server.start_server()
    print(f"Started DSTm server: running on 127.0.0.1:{port}")
    print(f"Is DTSS running: {dtss.is_running()}, on {dtss.get_listening_port()}")
    remote_dtss = True
    if remote_dtss:
        print(f"Use remote dtss, forward requests to 127.0.0.1:{dtss_port}")
        server.set_master_slave_mode(ip="127.0.0.1",
                                     port=dtss_port,
                                     master_poll_time=1,
                                     unsubscribe_threshold=2,
                                     unsubscribe_max_delay=2)
        time.sleep(2)

    else:
        server.add_container("stm", container.as_posix())

    stm = create_stm()
    dstm_client = DStmClient(f"127.0.0.1:{port}", 9999)
    dstm_client.add_model(stm.name, stm)
    dstm_client.clone_model(stm.name, "stm_last")
    #dstm_client.add_model(stm.name, stm)

    # Get model from modelservice
    get_stm = dstm_client.get_model(stm.name)
    # volume.schedule.value on model is not evaluted and should be unbound
    get_ts = get_stm.hydro_power_systems[0].reservoirs[0].volume.schedule.value
    assert get_ts.needs_bind()

    # Get model after evaluation
    dstm_client.evaluate_model(mid=stm.name, bind_period=ta.total_period(), use_ts_cached_read=True, update_ts_cache=False)
    # dstm_client.optimize()
    get_stm = dstm_client.get_model(stm.name)
    get_ts = get_stm.hydro_power_systems[0].reservoirs[0].volume.schedule.value
    assert not get_ts.needs_bind() # Model is  evaluated, ts is bound if found in dtss
    #assert get_ts == ts

    print(dstm_client.get_state(stm.name))

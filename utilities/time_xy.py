import shyft.time_series as sa
from shyft.energy_market.stm import t_xy, t_xyz, t_xyz_list
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveWithZ, XyPointCurveWithZList

cal = sa.Calendar("Europe/Oslo")
t_now = sa.utctime_now()
t_day = cal.trim(t_now, cal.DAY)
t_next_day = t_day + cal.DAY

def example_pq_curve():

    # A pq curve explains the relation between flowing water and energy production in a turbine

    t_pq_curve = t_xy()
    # The relation between water flow and unit production, m3/s -> MW
    flow_to_power = PointList([Point(10, 100), Point(20, 150), Point(30, 350)])
    xy_point_curve = XyPointCurve(flow_to_power)
    t_pq_curve[t_day] = xy_point_curve
    print(len(flow_to_power))
    print("An example PQ curve")
    for i in range(len(flow_to_power)-1):
        print(f"For flow {t_pq_curve[t_day].points[i].x} m/s, {t_pq_curve[t_day].points[i].y} MWh is produced")

    print("Print max value of xy list on t_day", t_pq_curve[t_day].x_max())
    # You can ask for a point outside defined boundary, then linear line of the two points at the edge is used
    print("Calculate_y(5): ",t_pq_curve[t_day].calculate_y(5))
    print("Calculate_y(10): ",t_pq_curve[t_day].calculate_y(10))
    print("Calculate_y(20): ",t_pq_curve[t_day].calculate_y(20))
    print("Calculate_y(30): ", t_pq_curve[t_day].calculate_y(30))
    print("Calculate_y(40): ", t_pq_curve[t_day].calculate_y(40))




if __name__ == "__main__":
    example_pq_curve()
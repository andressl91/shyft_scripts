from builtins import print

import shyft.time_series as sa


def print_qm_info(queue_message_info):
    print(f"{queue_message_info.msg_id=}")
    print(f"{queue_message_info.created=}")
    print(f"{queue_message_info.description=}")
    print(f"{queue_message_info.diagnostics=}")
    print(f"{queue_message_info.done=}")
    print(f"{queue_message_info.fetched=}")
    print(f"{queue_message_info.ttl=}")
    print()


cal = sa.Calendar("Europe/Oslo")


def create_tsv():
    t_now = sa.utctime_now()
    t_day = cal.trim(t_now, cal.DAY)
    ta = sa.TimeAxis(t_day, cal.DAY, 4)

    vals = [i for i in range(len(ta))]
    ts = sa.TimeSeries(ta, vals, point_fx=sa.POINT_AVERAGE_VALUE)
    tsv = sa.TsVector()
    tsv.append(ts)

    return tsv


srv = sa.DtsServer()
port = srv.start_async()

client = sa.DtsClient(f"127.0.0.1:{port}")
tsv = create_tsv()

# First add a queue to hold tsv
queue_name = "my_queue"
msg_id = "1"
client.q_add(queue_name)
# Print all queues on the dtss
print(client.q_list())

# Add tsv to queue
client.q_put(queue_name, msg_id, "{'name': 'test}", 5000, tsv)

# Print current qm_info for the queue
print(f"QueueMessageInfo after putting tsv to {queue_name}\n")
print_qm_info(client.q_msg_info(queue_name, msg_id))

# Print all msg infos
print([(i.msg_id, i.description) for i in client.q_msg_infos(queue_name)])

# Get tsv in queue
queue_info = client.q_get(queue_name, max_wait=1000)
print(f"QueueMessageInfo after getting tsv from {queue_name}\n")
print_qm_info(client.q_msg_info(queue_name, msg_id))

# Print queue msg_id and description
print(queue_info.info.msg_id, queue_info.info.description)
# Print the tsv arriving with the message from dtss queue
print(queue_info.tsv[0].v)

# Check that the tsv in queue is gone
print(client.q_size(queue_name))

client.q_ack(queue_name, msg_id=queue_info.info.msg_id, diagnostics="{'status': 'good_stuff'}")
print(f"QueueMessageInfo after acknowledging tsv to {queue_name}\n")
print_qm_info(client.q_msg_info(queue_name, msg_id))
# Print messageinfo attrs


srv.clear()

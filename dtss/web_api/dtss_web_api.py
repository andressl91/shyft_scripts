from pathlib import Path
from tempfile import TemporaryDirectory
import websockets
import asyncio

import shyft.time_series as sa


def api_host(port):
    return f"ws://127.0.0.1:{port}"


def get_response(req: str, port: int):
    async def wrap(wreq):
        async with websockets.connect(api_host(port)) as websocket:
            await websocket.send(wreq)
            response = await websocket.recv()
            return response

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    return loop.run_until_complete(wrap(req))

def get_url(container, path):
    return f"shyft://{container}/{path}"


def get_ta():
    cal = sa.Calendar("Europe/Oslo")
    t_now = sa.utctime_now()
    t_day = cal.trim(t_now, cal.DAY)
    #return sa.TimeAxisCalendarDeltaT(cal, t_now, t_day, 5)
    return sa.TimeAxis(t_now, cal.DAY, 5)


def store_tsv(port):
    client = sa.DtsClient(f"127.0.0.1:{port}")
    cal = sa.Calendar("Europe/Oslo")
    t_now = sa.utctime_now()
    t_day = cal.trim(t_now, cal.DAY)

    ta = get_ta()
    ts = sa.TimeSeries(ta, [i for i in range(len(ta))], point_fx=sa.POINT_AVERAGE_VALUE)
    ts = sa.TimeSeries(get_url(container="foo", path="bar.tsb"), ts)
    ts1 = sa.TimeSeries(ta, [i+10 for i in range(len(ta))], point_fx=sa.POINT_AVERAGE_VALUE)
    ts1 = sa.TimeSeries(get_url(container="foo", path="banana.tsb"), ts)
    tsv = sa.TsVector()
    tsv.extend([ts, ts1])

    client.store_ts(tsv)


def get_tsv(port):
    ts = sa.TimeSeries(get_url(container="foo", path="bar.tsb"))
    tsv = sa.TsVector()
    tsv.extend([ts])

    client = sa.DtsClient(f"127.0.0.1:{port}")
    tsv_eval = client.evaluate(tsv, get_ta().total_period())
    print(tsv_eval[0].v)

def find_web_api(port):

    req = """find {"request_id" : "foo", "find_pattern": "shyft://foo/b*" } """

    r = get_response(req, port)
    print(r)

def evaluate_web_api(port):
    print("Evaluate web api")
    ta = get_ta()
    start = int(ta.total_period().start.seconds)
    end = int(ta.total_period().end.seconds)
    ts_url = get_url("foo", "bar.tsb")
    req = """
            read {
            "request_id": "my_eval",
            "read_period": [%s, %s],
            "clip_period": [%s, %s],
            "cache": true,
            "ts_ids": ["%s"],
            "subscribe": false,
            "ts_fmt": false
            }
          """ % (start, end, start, end, ts_url)
    req2 = """
            read {
            "request_id": "my_eval",
            "read_period": [%s, %s],
            "clip_period": [%s, %s],
            "cache": true,
            "ts_ids": ["%s"],
            "subscribe": false,
            "ts_fmt": true        
            }
          """ % (start, end, start, end, ts_url)
    print("The request", req)
    print("Response, with ts_fmt: false")
    r = get_response(req, port)
    print(r)
    print("Response, with ts_fmt: true")
    r = get_response(req2, port)
    print(r)


with TemporaryDirectory() as tmp:
    dtss_root = Path(tmp)
    dtss = sa.DtsServer()
    container = "foo"
    dtss.set_container(container, tmp)
    port = dtss.start_async()
    web_api_port = port + 1
    dtss.start_web_api("127.0.0.1", web_api_port, tmp)

    # Socket store/evaluate
    store_tsv(port)
    get_tsv(port)

    # Web find
    find_web_api(web_api_port)

    # Web evaluate
    evaluate_web_api(web_api_port)

    dtss.stop_web_api()
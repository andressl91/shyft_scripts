import shyft.time_series as sa

cal = sa.Calendar("Europe/Oslo")

t_now = sa.utctime_now()
t_start = cal.trim(t_now, cal.WEEK)
tah = sa.TimeAxis(t_start, cal.HOUR, 3)
tam = sa.TimeAxis(t_start, cal.MINUTE, 120)

tsh = sa.TimeSeries(tah, [i for i in range(len(tah.time_points)-1)], point_fx=sa.POINT_AVERAGE_VALUE)
print(tsh.v.to_numpy())
print()

tsm = tsh.average(tam)
print(tsm.v.to_numpy(), "")
print()

ts_avg = tsh.statistics(tam, sa.POINT_AVERAGE_VALUE)
print(ts_avg.v.to_numpy())

from shyft.energy_market.stm import StmTask, StmCase, StmModelRef, ModelRefList
from shyft.time_series import utctime_now

def explore_task():

    task = StmTask(id=1, name="operative_task", created=utctime_now(), json="{}",
    labels="My label", task_name="no1_price_forcast")

    # Create a StmModelRef for the StmTasks base-model, can also be done in the constructor, arg=base_model
    base_model_ref = StmModelRef("127.0.0.1", port_num=1234, api_port_num=5678, model_key="model_id")
    task.base_model = base_model_ref

    # Relate StmCases to the StmTask, and a modelref to the case
    case = StmCase(id=1, name="morning_forecast", created=utctime_now(), json="{}", labels="Operative")
    case_model_ref = StmModelRef("127.0.0.1", port_num=1234, api_port_num=5678, model_key="no1_morning_price_forecast")
    
    # Add StmModelRef to the StmCase, a StmCase can have multiple model refs
    case.add_model_ref(case_model_ref)

    case_model_ref_2 = StmModelRef("127.0.0.1", port_num=1234, api_port_num=5678, model_key="no2_morning_price_forecast")
    case.add_model_ref(case_model_ref_2)
    print(len(case.model_refs))

    # Now, one can fetch a model ref from the case
    mr = case.get_model_ref("no2_morning_price_forecast")

    # Can also remove a model ref from the case
    case.remove_model_ref("no2_morning_price_forecast")
    print(len(case.model_refs))


    # A list model ModelRefs can be given in the constructor
    
    mfl = ModelRefList()
    mfl.append(case_model_ref)
    mfl.append(case_model_ref_2)
    case = StmCase(id=2, name="afternoon_forecast", created=utctime_now(), json="{}", labels="Operative", model_refs=mfl)

if __name__ == "__main__":
    explore_task()
from tempfile import TemporaryDirectory
from pathlib import Path
from shyft.energy_market.stm import  StmSystem, HydroPowerSystem, StmServer, StmClient, ModelInfo, DStmServer, DStmClient
import shyft.time_series as sa

def shyft_url_water_level():
    return "model_id/water"

def create_hps(hps):
    cal = sa.Calendar()
    t_now = sa.utctime_now()
    t_day = cal.trim(t_now, cal.DAY)
    # ------------- Units -------------
    unit1 = hps.create_unit(1, 'unit1')

    # ---------- Reservoir ----------
    rsv = hps.create_reservoir(1, "res1")

    # Lowest regulated water level
    ta = sa.TimeAxis(t_now, t_now + 10 * cal.YEAR, 1)
    high = sa.TimeSeries(ta, [200], sa.POINT_AVERAGE_VALUE)
    #low = sa.TimeSeries(ta, [165], sa.POINT_AVERAGE_VALUE)
    low = sa.TimeSeries("shyft://foo/bar")*3 - sa.TimeSeries("shyft://correction_water")
    rsv.level.constraint.min.value = low
    rsv.level.constraint.max.value = high
    # Highest regulated water level


hps = HydroPowerSystem(1, "hps")
hps = create_hps(hps)
stm = StmSystem(1, "stm_sys", json='')
stm.hydro_power_systems.append(hps)

with TemporaryDirectory() as tmp:
    stms = StmServer(tmp)
    #stms.set_listening_port(2222)
    port = stms.start_server()


    mi = ModelInfo(id=stm.id, name=stm.name, created=sa.utctime_now(), json="{labels:{operational, leirdola}}")
    stm_client = StmClient(f"localhost:{port}", 9999)

    stm_client.store_model(stm, mi)

    stm = stm_client.read_model(mid=mi.id)

    dtsm_server = DStmServer()
    port = dtsm_server.start_server()
    dtsm_server.start_web_api("127.0.0.1", 9000, doc_root="/tmp/foo")

    dstm_client = DStmClient(f"127.0.0.1:{port}", 9999)
    dstm_client.add_model("spam", stm)
    stm = dstm_client.get_model("spam")
    print("HERE")
    dtsm_server.close()
    dtsm_server.stop_web_api()
    # dstm_client.clone_model("spam", "egg")
    # dstm_client.evaluate_model("spam")
    # dstm_client.optimize("spam")

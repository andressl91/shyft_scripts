import sys
import argparse

from shyft.energy_market import stm
from shyft.energy_market import ui
from shyft import time_series as sa
from PySide2.QtWidgets import QApplication

from layout_callbacks import LayoutCallbacks

from shyft.time_series import Calendar, TsVector, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE
from shyft.time_series import utctime_now, shyft_url
from shyft.time_series import DtsServer, DtsClient  # pylint: disable=no-name-in-module

# Create a QApplication singleton first
app = QApplication([])


class LayoutService:
    def __init__(self, interface: str, web_interface: str, port_num, web_port, doc_root):

        self.interface = interface
        self.web_interface = web_interface
        self.doc_root = doc_root
        self.srv = ui.LayoutServer(self.doc_root)
        self.srv.set_listening_port(port_num)
        self.srv.fx = LayoutCallbacks()
        self.client = None
        self.api_port = web_port

    def __enter__(self):
        self.srv.start_server()
        self.srv.start_web_api(self.web_interface, self.api_port, self.doc_root + "/web", 1, 1)
        self.client = ui.LayoutClient(host_port=f"{self.interface}:{self.srv.get_listening_port()}", timeout_ms=1000)

        print(f"\nLayoutServer serving on '{self.interface}:{self.srv.get_listening_port()}', with web-api on '{self.web_interface}:{self.api_port}'")

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
        # self.srv.stop_web_api()
        # self.srv.stop_server(1000)
        # self.client.close()


class DtssServer:
    def __init__(self, interface: str, web_interface: str, dtss_port: int, dtss_api_port: int, dtss_root: str):
        self.port = dtss_port
        self.interface = interface
        self.api_interface = web_interface
        self.api_port = dtss_api_port
        self.server_root = dtss_root
        self.container_name = "demo"
        self.dtss_path = self.server_root + "/" + self.container_name
        self.server = DtsServer()
        # self.client = DtsClient(f"{interface}:{self.port}")
        self.client = DtsClient(f"{interface}:{self.port}")

    def __enter__(self):
        self.server.set_listening_port(self.port)
        # self.server.set_auto_cache(True)
        self.server.set_container(self.container_name, self.dtss_path)
        self.server.start_async()
        self.server.set_can_remove(True)
        self.server.start_web_api(self.api_interface, self.api_port, self.server_root + "/web", 1, 1)

        cal = Calendar("UTC")
        t0 = cal.trim(utctime_now(), cal.DAY)
        ta = TimeAxis(t0, Calendar.HOUR, 24*7)
        ts1 = TimeSeries(ta, fill_value=1, point_fx=POINT_AVERAGE_VALUE)
        ts2 = TimeSeries(ta, fill_value=2, point_fx=POINT_AVERAGE_VALUE)
        tsv_example = TsVector([TimeSeries(shyft_url(self.container_name, "my_ts1.sdb"), ts1),
                                TimeSeries(shyft_url(self.container_name, "my_ts2.sdb"), ts2)])
        self.client.store_ts(tsv_example)

        print(f"\nDtsServer serving on '{self.interface}:{self.port}', with web-api on '{self.api_interface}:{self.api_port}'")

        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # self.server.stop_web_api()
        # self.server.clear()
        pass


if __name__ == "__main__":

    interface = "127.0.0.1"
    web_interface = "127.0.0.1"
    port = 4444
    api_port = 4445

    dtss_interface = "127.0.0.1"
    dtss_web_interface = "127.0.0.1"
    dtss_port = 5507
    dtss_web_port = 5508

    parser = argparse.ArgumentParser(description="Argument parser for start Layout service")
    parser.add_argument('--interface', metavar="interface", type=str, nargs="+",
                        help="Host for server", default=interface)
    parser.add_argument('--web_interface', metavar="web_interface", type=str, nargs="+",
                        help="Host for server", default=web_interface)
    parser.add_argument('--port-num', metavar="port_num", type=int, nargs="+",
                        help="Port number for server", default=port)
    parser.add_argument('--doc-root', metavar='doc_root', type=str, nargs='+',
                        help="Document root to serve layouts", default="./db-config-files")
    parser.add_argument('--web-port', metavar='web_port', default=api_port,
                        help="Port number for web API", type=int, nargs='+')

    parser.add_argument('--dtss-interface', metavar='dtss_interface', type=str, nargs='+',
                        help="Root folder of DtsS", default=dtss_interface)
    parser.add_argument('--dtss-web-interface', metavar='dtss_web_interface', type=str, nargs='+',
                        help="Root folder of DtsS", default=dtss_web_interface)
    parser.add_argument('--dtss-root', metavar='dtss_root', type=str, nargs='+',
                        help="Root folder of DtsS", default="./dtss_root")
    parser.add_argument('--dtss-port', metavar='dtss_port', type=int, nargs='+',
                        help="Port number for DtsS", default=dtss_port)
    parser.add_argument('--dtss-api-port', metavar="dtss_api_port", default=dtss_web_port,
                        help="Port number for web API for Dtss", type=int, nargs='+')

    args = parser.parse_args()

    with DtssServer(args.dtss_interface, args.dtss_web_interface, args.dtss_port, args.dtss_api_port, args.dtss_root) as dts_server:
        with LayoutService(args.interface, args.web_interface, args.port_num, args.web_port, args.doc_root) as layout_server:
            _ = input('\nPress return to close .... ')
            print(f'{"-" * 20} closing down {"-" * 20} \n')

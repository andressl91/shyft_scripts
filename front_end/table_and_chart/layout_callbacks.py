from shyft.energy_market import ui
from shyft.energy_market import stm
import functools
from typing import Callable
import random
import json
import copy
from PySide2.QtCore import Qt, QDate, QTime, QDateTime
from PySide2.QtGui import QFont
from PySide2.QtWidgets import QApplication, QWidget, QFrame, QLabel, \
    QPushButton, QVBoxLayout, QTableWidget, \
    QTableWidgetItem, QGridLayout, QMenu, QAction
from PySide2.QtCharts import QtCharts
from navigation_menu import NavigationMenu


class LayoutCallbacks:

    def __call__(self, name: str, args: str):
        print(f"Trying to generate a new layout with name: {name}")
        try:
            argmap = json.loads(args)
        except json.JSONDecodeError:
            raise RuntimeError("Unable to parse args as a JSON-struct.")
        tpe = argmap.pop("type")  # Should raise a KeyError if not present
        argmap_data = argmap.pop("data", {})

        callback_func: Callable = getattr(NavigationMenu(), tpe)
        qt_obj = callback_func(argmap_data)

        return ui.export(qt_obj)  # Post-processing of Widget-generating function to return a JSON-string decoding its data.

import json
from typing import List, Optional, Tuple
from PySide2.QtCore import Qt, QDate, QTime, QDateTime, QPointF
from PySide2.QtGui import QFont, QColor, QPen
from PySide2.QtCharts import QtCharts
from PySide2.QtWidgets import QApplication, QWidget, QFrame, QLabel,\
    QPushButton, QVBoxLayout, QTableWidget,\
    QTableWidgetItem, QGridLayout, QMenu, QAction

from shyft.time_series import TimeAxis, Calendar
from shyft.time_series import shyft_url, time, utctime_now
from shyft.energy_market import ui
from shyft.energy_market.ui import ItemDataProperty


def create_table_display_name(*, object_name: str, series_type: str) -> str:
    """
    Returns a string for table row header.

    Args:
        object_name: Name of object eg. Leirdøla G1
        series_type: Name of dataseries type eg. Level

    """
    return json.dumps({'object_name': object_name, 'series_type': series_type}, ensure_ascii=False)


def create_value_axis(axis_name='', limits: Tuple[Optional[float], Optional[float]] = (None, None)) -> QtCharts.QValueAxis:
    axis = QtCharts.QValueAxis()
    axis.setTitleText(axis_name)
    axis.setTickCount(7)
    if limits == (None, None):
        axis.setProperty('auto_scale', True)
    else:
        axis.setRange(limits[0], limits[1])

    return axis


class Callback:

    def __init__(self, request_id: str, layout_id: int, cb_name: str, layout_name=''):
        self.request_id = request_id
        self.layout_id = layout_id
        self.service = "layout"
        self.cb_name = cb_name
        self.layout_name = layout_name

    def to_json(self) -> dict:
        d_query = {
            'request_id': self.request_id,
            'layout_id': self.layout_id,
            'name': self.layout_name if self.layout_name else self.cb_name,
            'args': {
                'type': self.cb_name}
        }

        d_callback = {
            'query': f"read_layout {json.dumps(d_query, ensure_ascii=False)}",
            'service': self.service
        }

        return d_callback


class MenuItem:

    def __init__(self, id: int, text, icon: str = "", tag: str = "", callbacks: List[Callback] = None, style=""):
        self.id = id
        self.text = text
        self.icon = icon
        self.tag = tag
        self.callbacks = callbacks
        self.children = []
        self.window = None

    def build(self, window, presenter):

        self.window = window
        button = QPushButton(self.window)
        button.setProperty("presenter", presenter)
        self._update(button, self)

        self._build_children(button, self.children)
        return button

    def _build_children(self, parent, children):
        if len(children) > 0:
            q_menu = QMenu(self.window)
            parent.setMenu(q_menu)

            for i in children:
                action = QAction(self.window)
                self._update(action, i)
                q_menu.addAction(action)

                if i.children:
                    self._build_children(action, i.children)

    def _update(self, component, props):
        component.setText(props.text)
        component.setProperty("id", props.id)
        component.setProperty("tag", props.tag)
        component.setProperty("callbacks", [cb.to_json() for cb in props.callbacks] if props.callbacks else [])
        component.setProperty("icon_name", props.icon)
        component.setProperty("view_identifier", props.id)  # 'front end only?'
        # component.setProperty("default_selection", 12345)   # 'front end only?'


class NavigationMenu:

    def __init__(self):
        self.shyft_container = "demo"

    def navigation_start_up(self, argmap):
        window = QWidget()
        layout = QVBoxLayout()
        window.setLayout(layout)

        view_menu = self.get_view_menu(argmap)
        layout.addWidget(view_menu.build(window, "expand-panel"))
        return window

    def get_view_menu(self, argmap):

        callback = Callback(request_id="midfield", layout_id=0, cb_name="get_view")

        views = MenuItem(0, "select view", "", "view", )
        one = MenuItem(1, "view one", "flight_land", "view", callbacks=[callback])
        two = MenuItem(2, "view two", "flight_land", "view", callbacks=[callback])
        views.children.extend((one, two))
        return views

    def get_view(self, argmap):
        view = QWidget()
        layout = QVBoxLayout()
        view.setLayout(layout)

        view_id = argmap['view']
        cal = Calendar("UTC")
        t0 = cal.trim(utctime_now(), Calendar.DAY)
        ta = TimeAxis(t0, Calendar.HOUR, 24*3)

        chart = self.create_chart1()
        if view_id == 1:
            table = self._create_table1()
        elif view_id == 2:
            table = self._create_table2()
        else:
            raise RuntimeError(f"Unknown view_id: {view_id}")

        labels = [str(tp) for tp in ta.time_points_double]
        table.setColumnCount(len(labels))
        table.setHorizontalHeaderLabels(labels)

        for s in chart.series():
            s.replace([QPointF(t, 0) for t in ta.time_points_double])

        layout.addWidget(QtCharts.QChartView(chart))
        layout.addWidget(table)

        return view

    def create_chart1(self):
        chart = QtCharts.QChart()
        chart.setTitle("Chart title")
        chart.setBackgroundBrush(QColor(192, 192, 192, 255))  #TODO angular: will crach if not set.

        x_axis, y_axis = QtCharts.QDateTimeAxis(), create_value_axis(axis_name='Y-axis')
        chart.addAxis(x_axis, Qt.AlignBottom)
        chart.addAxis(y_axis, Qt.AlignRight)

        s1, s2 = QtCharts.QLineSeries(), QtCharts.QLineSeries()
        s1.setName("Name type1")
        s2.setName("Name2 type2")

        pen1 = QPen()
        pen1.setColor('black')
        pen2 = QPen()
        pen2.setColor('blue')

        s1.setPen(pen1)
        s2.setPen(pen2)

        s1.setProperty('data_y', shyft_url(self.shyft_container, "my_ts1.sdb"))
        s2.setProperty('data_y', shyft_url(self.shyft_container, "my_ts2.sdb"))
        s1.setProperty('scale_factor_y', 1.0)
        s2.setProperty('scale_factor_y', 1.0)

        chart.addSeries(s1)
        chart.addSeries(s2)

        s1.attachAxis(x_axis)
        s1.attachAxis(y_axis)

        s2.attachAxis(x_axis)
        s2.attachAxis(y_axis)

        return chart

    def _create_table1(self):
        table = QTableWidget()
        table.setColumnCount(1)
        table_item = QTableWidgetItem(create_table_display_name(object_name="Name", series_type="type1"))
        table_item.setData(ItemDataProperty.DataY, shyft_url(self.shyft_container, "my_ts1.sdb"))
        table.setRowCount(1)
        table.setVerticalHeaderItem(0, table_item)
        return table

    def _create_table2(self):
        table = QTableWidget()
        table.setColumnCount(1)
        table_item1 = QTableWidgetItem(create_table_display_name(object_name="Name", series_type="type1"))
        table_item1.setData(ItemDataProperty.DataY, shyft_url(self.shyft_container, "my_ts1.sdb"))
        table_item2 = QTableWidgetItem(create_table_display_name(object_name="Name2", series_type="type2"))
        table_item2.setData(ItemDataProperty.DataY, shyft_url(self.shyft_container, "my_ts2.sdb"))
        table.setRowCount(2)
        table.setVerticalHeaderItem(0, table_item1)
        table.setVerticalHeaderItem(1, table_item2)
        return table
